#include <rtthread.h>
#include <rtdevice.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFF_SIZE 20
#define LED_PIN 25
#define BUZZ_PIN 16

//PWM
#define PWM_DEV_NAME "pwm0"
#define PWM_1A 2 //pin 4
#define PWM_1B 3 //pin 5

//ADC
#define ADC_DEV_NAME "adc0"
#define ADC_GPIO_1 26
#define ADC_GPIO_2 27

#define UART_DEV_NAME "uart0"

//Thread parametri
#define THREAD_PRIORITY       6
#define THREAD_STACK_SIZE     1024
#define THREAD_TIMESLICE      5

//Granice
#define DEF_LOW_BOUND 2050
#define DEF_UPP_BOUND 3000

#define DEF_BUZZ_FAST 500
#define DEF_BUZZ_SLOW 1500

#define BUZZ_FREQ_MAX 3000
#define BUZZ_FREQ_MIN 200

#define UPPER_BOUND_MIN 2000
#define UPPER_BOUND_MAX 3000

#define LOWER_BOUND_MAX 3000
#define LOWER_BOUND_MIN 2100

#define SENSING_BOUND 2000

//Tredovi
static rt_thread_t buzzer;
static rt_thread_t console_input;
static rt_thread_t decision_maker;
static rt_thread_t sensor_a_input;
static rt_thread_t sensor_b_input;

//Devices
struct rt_device_pwm *pwm_dev;
static rt_adc_device_t adc_dev;
static rt_device_t uart_dev;

//ADC values
static rt_uint32_t adc_val_a = 0;
static rt_uint32_t adc_val_b = 0;

//Parameters for sensor PWM, do not touch this
static const rt_uint64_t pwm_period_sensor = 11;
static const rt_uint64_t pwm_pulse_sensor = 6;

//sleep za buzzer
static rt_uint64_t buzz_cycle=0;

//flag za buzzer
static rt_uint16_t buzz_flag=0;

//frekvencije pistanja
static rt_uint16_t buzz_fast=500;//podesava korisnik
static rt_uint16_t buzz_slow=1500;//podesava korisnik

//mutex za parametre pistanja
static rt_mutex_t param_mux;

//mutex za ukljucenje senzora
static rt_mutex_t power_mux;

//granice
static rt_uint16_t upper_bound=2200;//podesava korisnik
static rt_uint16_t lower_bound=3100;//podesava korisnik

static rt_uint16_t buffer_a[10];

static void sensor_a_thread_entry(void* param)
{
    while(1)
    {
        rt_mutex_take(power_mux, RT_WAITING_FOREVER);

            rt_pwm_enable(pwm_dev, PWM_1A);
            rt_mutex_take(param_mux, RT_WAITING_FOREVER);
                rt_thread_mdelay(1);
                adc_val_a=rt_adc_read(adc_dev, ADC_GPIO_1);
            rt_mutex_release(param_mux);

            rt_pwm_disable(pwm_dev, PWM_1A);

        rt_mutex_release(power_mux);

        rt_thread_mdelay(100);
    }
}

static void sensor_b_thread_entry(void* param)
{
    while(1)
    {
        rt_mutex_take(power_mux, RT_WAITING_FOREVER);

            rt_pwm_enable(pwm_dev, PWM_1B);

            rt_mutex_take(param_mux, RT_WAITING_FOREVER);
                rt_thread_mdelay(1);
                adc_val_b=rt_adc_read(adc_dev, ADC_GPIO_2);
            rt_mutex_release(param_mux);
            rt_pwm_disable(pwm_dev, PWM_1B);
        rt_mutex_release(power_mux);
        rt_thread_mdelay(100);
    }
}

static void decision_maker_entry(void* param)
{
    while(1)
    {
        rt_mutex_take(param_mux, RT_WAITING_FOREVER);
            if((adc_val_a>upper_bound && adc_val_a<lower_bound) || (adc_val_b>upper_bound && adc_val_b<lower_bound))
            {
                if(adc_val_a>upper_bound+(lower_bound-upper_bound)/2 || adc_val_b>upper_bound+(lower_bound-upper_bound)/2)
                {
                    buzz_cycle=buzz_fast;
                }
                else
                {
                    buzz_cycle=buzz_slow;
                }

                buzz_flag=1;
            }
            else
            {
                buzz_flag=0;
            }
        rt_mutex_release(param_mux);

        rt_thread_mdelay(50);
    }
}

static void buzz_entry(void* param)
{
    while(1)
    {
        if(buzz_flag==1)
        {
           rt_pin_write(LED_PIN, 1);
           rt_pin_write(BUZZ_PIN, 1);
           rt_thread_mdelay(buzz_cycle);
           rt_pin_write(LED_PIN, 0);
           rt_pin_write(BUZZ_PIN, 0);
           rt_thread_mdelay(buzz_cycle);
        }
        else if(buzz_flag==0)
        {
            rt_pin_write(LED_PIN, 0);
            rt_pin_write(BUZZ_PIN, 0);
            rt_thread_mdelay(100);
        }
    }
}

static int init_adc()
{
    adc_dev = (rt_adc_device_t) rt_device_find(ADC_DEV_NAME);

    if (adc_dev == RT_NULL)
    {
        return -1;
    }

    rt_adc_enable(adc_dev, ADC_GPIO_1);
    rt_adc_enable(adc_dev, ADC_GPIO_2);

    return 0;
}

static int init_pwm()
{
    pwm_dev = (struct rt_device_pwm *) rt_device_find(PWM_DEV_NAME);

    if (pwm_dev == RT_NULL)
    {
        return -1;
    }

    rt_pwm_set(pwm_dev, PWM_1A, pwm_period_sensor, pwm_pulse_sensor);
    rt_pwm_set(pwm_dev, PWM_1B, pwm_period_sensor, pwm_pulse_sensor);

    return 0;
}

static void console_input_entry(void* param)
{
    rt_kprintf("************************************\n");
    rt_kprintf("************************************\n");
    rt_kprintf("***SISTEM ZA POMOC PRI PARKIRANJU***\n");
    rt_kprintf("************************************\n");
    rt_kprintf("************************************\n");
    rt_kprintf("Dozvoljeni opsezi:\n");
    rt_kprintf("Frekvencije zvuka: 200-3000[ms]\n");
    rt_kprintf("Gornja granica detektibilnosti: 2000-2700\n");
    rt_kprintf("Donja150 granica detektibilnosti: 2300-3000\n");
    rt_kprintf("Parametre unesite u formatu: [frekv_zvuka_sporo] [frekv_zvuka_brzo] [donja_granica] [gornja_granica]\n");

    uart_dev=rt_device_find(UART_DEV_NAME);
    rt_device_open(uart_dev, RT_DEVICE_FLAG_INT_RX);

    rt_uint16_t new_buzz_slow=0;
    rt_uint16_t new_buzz_fast=0;
    rt_uint16_t new_lower_bound=0;
    rt_uint16_t new_upper_bound=0;

    char input_buff[BUFF_SIZE+1];

    char pom;

    int i=0;
    char* pok_point;

    while (1)
    {
        rt_kprintf("\nUnesite parametre(BZ slow: %d, BZ fast: %d, LWB: %d, UPB: %d):\n",buzz_slow,buzz_fast,lower_bound,upper_bound);

        rt_thread_mdelay(60000);

        i=rt_device_read(uart_dev, -1, input_buff, BUFF_SIZE);

        input_buff[BUFF_SIZE]='\0';

        if(i>0)
        {
            new_buzz_slow=strtol(input_buff,&pok_point,10);
            new_buzz_fast=strtol(pok_point,&pok_point,10);
            new_lower_bound=strtol(pok_point,&pok_point,10);
            new_upper_bound=strtol(pok_point,&pok_point,10);

            if(new_buzz_slow>=BUZZ_FREQ_MIN && new_buzz_slow<=BUZZ_FREQ_MAX)
            {
                if(new_buzz_fast>=BUZZ_FREQ_MIN && new_buzz_fast<=BUZZ_FREQ_MAX)
                {
                    if(new_lower_bound>=LOWER_BOUND_MIN && new_lower_bound<=LOWER_BOUND_MAX)
                    {
                        if(new_upper_bound>=UPPER_BOUND_MIN && new_upper_bound<=UPPER_BOUND_MAX)
                        {
                            if(new_lower_bound>new_upper_bound)
                            {
                                rt_mutex_take(param_mux, RT_WAITING_FOREVER);
                                    upper_bound=new_upper_bound;
                                    lower_bound=new_lower_bound;
                                    buzz_fast=new_buzz_fast;
                                    buzz_slow=new_buzz_slow;
                                rt_mutex_release(param_mux);
                            }
                        }
                    }
                }
            }
        }
    }

}

int main()
{
    init_adc();
    init_pwm();

    rt_pin_mode(BUZZ_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LED_PIN, PIN_MODE_OUTPUT);

    param_mux=rt_mutex_create("Mutex", RT_IPC_FLAG_FIFO);
    power_mux=rt_mutex_create("ON_OFF", RT_IPC_FLAG_FIFO);

    sensor_a_input=rt_thread_create("sensor1", sensor_a_thread_entry, RT_NULL, THREAD_STACK_SIZE, THREAD_PRIORITY,THREAD_TIMESLICE);
    sensor_b_input=rt_thread_create("sensor2", sensor_b_thread_entry, RT_NULL, THREAD_STACK_SIZE, THREAD_PRIORITY,THREAD_TIMESLICE);

    decision_maker=rt_thread_create("odlucivac", decision_maker_entry, RT_NULL, THREAD_STACK_SIZE, THREAD_PRIORITY, THREAD_TIMESLICE);

    buzzer=rt_thread_create("buzzer", buzz_entry, RT_NULL, THREAD_STACK_SIZE, THREAD_PRIORITY,THREAD_TIMESLICE);

    console_input=rt_thread_create("konzola", console_input_entry, RT_NULL, THREAD_STACK_SIZE, THREAD_PRIORITY, THREAD_TIMESLICE);

    rt_thread_startup(console_input);
    rt_thread_startup(sensor_a_input);
    rt_thread_startup(sensor_b_input);
    rt_thread_startup(buzzer);
    rt_thread_startup(decision_maker);

    return 0;
}



