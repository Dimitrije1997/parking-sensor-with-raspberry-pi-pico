#include <rtdevice.h>
#include <rtthread.h>
#include "board.h"
#include "drv_adc.h"
#include "hardware/adc.h"

#define ADC_DEVICE_NAME "adc0"

//dostupni pinovi za ADC konverziju
#define ADC0 26
#define ADC1 27
#define ADC2 28

#define SKT_ADC_DEVICE(adc_dev)    (struct skt_adc_dev *)(adc_dev)

static struct skt_adc_dev adc_dev0;

struct skt_adc_dev
{
    struct rt_adc_device parent;
    rt_uint32_t adc_periph;
    rt_uint32_t channel;
};

static rt_err_t skt_adc_enabled(struct rt_adc_device *device, rt_uint32_t channel, rt_bool_t enabled)
{
    rt_err_t ret = RT_EOK;
    return ret;
}
static rt_err_t skt_adc_convert(struct rt_adc_device *device, rt_uint32_t channel, rt_uint32_t *value)
{
    rt_err_t ret = RT_EOK;
    struct skt_adc_dev *adc = SKT_ADC_DEVICE(device);
    RT_ASSERT(adc != RT_NULL);

    if(channel==ADC0)
    {
        adc_select_input(0);
        *value=adc_read();
    }
    else if (channel==ADC1)
    {
        adc_select_input(1);
        *value=adc_read();
    }
    else if(channel==ADC2)
    {
        adc_select_input(2);
        *value=adc_read();
    }

    return ret;
}

const static struct rt_adc_ops skt_adc_ops =
{
    skt_adc_enabled,
    skt_adc_convert
};

int rt_hw_adc_init(void)
{
    rt_err_t ret = RT_EOK;

    adc_init();
    adc_gpio_init(26);
    adc_gpio_init(27);
    adc_gpio_init(28);

    ret = rt_hw_adc_register(&adc_dev0.parent, ADC_DEVICE_NAME, &skt_adc_ops, &adc_dev0);

    return ret;
}
INIT_DEVICE_EXPORT(rt_hw_adc_init);
