#include <rtdevice.h>
#include <rtthread.h>
#include "drv_pwm.h"
#include "pico/stdlib.h"
#include "hardware/pwm.h"

//Ime pwm device-a
#define PWM_DEVICE_NAME "pwm0"

#define SKT_PWM_DEVICE(pwm)    (struct skt_pwm_dev *)(pwm)

static struct skt_pwm_dev pwm_dev0;

struct skt_pwm_dev
{
    struct rt_device_pwm parent;
    rt_uint32_t pwm_periph;
};

static rt_err_t skt_pwm_enable(void *user_data, struct rt_pwm_configuration *cfg, rt_bool_t enable)
{
    rt_err_t ret = RT_EOK;
    rt_uint32_t slice=pwm_gpio_to_slice_num(cfg->channel);

    pwm_set_enabled(slice, enable);

    return ret;
}

static rt_err_t skt_pwm_get(void *user_data, struct rt_pwm_configuration *cfg)
{
    rt_err_t ret = RT_EOK;
    return ret;
}
static rt_err_t skt_pwm_set(void *user_data, struct rt_pwm_configuration *cfg)
{
    rt_err_t ret = RT_EOK;
    rt_uint32_t slice=pwm_gpio_to_slice_num(cfg->channel);
    rt_uint32_t channel=pwm_gpio_to_channel(cfg->channel);

    gpio_set_function(cfg->channel, GPIO_FUNC_PWM);
    pwm_set_wrap(slice, cfg->period);
    pwm_set_chan_level(slice, channel, cfg->pulse);
    pwm_set_clkdiv(slice, 255);

    return ret;
}

static rt_err_t skt_pwm_control(struct rt_device_pwm *device, int cmd, void *arg)
{
    rt_err_t ret = RT_EOK;
    struct skt_pwm_dev *pwm = SKT_PWM_DEVICE(device->parent.user_data);
    struct rt_pwm_configuration *cfg = (struct rt_pwm_configuration *)arg;

    RT_ASSERT(pwm != RT_NULL);

    switch (cmd)
    {
    case PWM_CMD_ENABLE:

        ret = skt_pwm_enable((void *)pwm->pwm_periph, cfg, RT_TRUE);
        break;
    case PWM_CMD_DISABLE:

        ret = skt_pwm_enable((void *)pwm->pwm_periph, cfg, RT_FALSE);
        break;
    case PWM_CMD_SET:

        ret = skt_pwm_set((void *)pwm->pwm_periph, cfg);
        break;
    case PWM_CMD_GET:

        ret = skt_pwm_get((void *)pwm->pwm_periph, cfg);
        break;
    default:
        ret = RT_EINVAL;
        break;
    }

    return ret;
}

const static struct rt_pwm_ops skt_pwm_ops =
{
    skt_pwm_control
};

int rt_hw_pwm_init(void)
{
    rt_err_t ret = RT_EOK;
    ret = rt_device_pwm_register(&pwm_dev0.parent, PWM_DEVICE_NAME, &skt_pwm_ops, &pwm_dev0);

    return ret;
}
INIT_DEVICE_EXPORT(rt_hw_pwm_init);
