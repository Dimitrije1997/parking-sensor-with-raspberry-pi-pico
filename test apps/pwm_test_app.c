#include <rtthread.h>
#include <rtdevice.h>

#define PWM_DEV_NAME "pwm0"  //ime device-a
#define PWM_DEV_CHANNEL 16 //GPIO za testiranje

//Mapiranje izmedju GPIO pinova i izlaznih kanala odredjenih slice-ova PWM periferije

//GPIO za transducer-e prednjeg senzora
#define PWM_0A 0 //pin 1
#define PWM_0B 1 //pin 2

//GPIO za transducer-e zadnjeg senzora
#define PWM_1A 2 //pin 4
#define PWM_1B 3 //pin 5

//GPIO za bazer
#define PWM_2A 4 //pin 6

struct rt_device_pwm *pwm_dev; //device handler

int main(void)
{
    rt_uint64_t period, pulse, dir;

    //Vrijednost perioda predstavlja koliko puta brojac PWM-a treba da odbroji prije nego sto se resetuje
    //i pocne novi period. Za clkdiv=255 koliko je dovoljno za potrebe projekta pri period=65500 dobija se
    //frekvencija od priblizno 7.5[Hz]. Faktor popune je proizvoljan
    period = 65500;
    pulse = 10000;

    pwm_dev = (struct rt_device_pwm *)rt_device_find(PWM_DEV_NAME);
    if (pwm_dev == RT_NULL)
    {
        rt_kprintf("pwm sample run failed! can't find %s device!\n", PWM_DEV_NAME);
        return RT_ERROR;
    }

    //Postavljanje perioda
    rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);
    //Omogucavanje rada pwm device-a
    rt_pwm_enable(pwm_dev, PWM_DEV_CHANNEL);

    //Demonstracija povecanja faktora popune
    while (1)
    {
        rt_thread_mdelay(5000);
        if(pulse<period)
        {
            pulse+=5000;
        }
        else
        {
            pulse=0;
        }
        rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);
    }
}
