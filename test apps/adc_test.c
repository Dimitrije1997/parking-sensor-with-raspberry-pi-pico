#include <rtthread.h>
#include <rtdevice.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"

#define LED_PIN 25

#define ADC_DEV_NAME "adc0"
#define ADC_DEV_CHANNEL 1
#define ADC_PIN 27

int main() {

    stdio_init_all();

    rt_pin_mode(LED_PIN, PIN_MODE_OUTPUT);

    rt_adc_device_t adc_dev;
    rt_uint32_t t;

    adc_dev = (rt_adc_device_t)rt_device_find(ADC_DEV_NAME);

    if(adc_dev==RT_NULL)
    {
        rt_kprintf("Registracija drajvera nije uspjela!");
        return -1;
    }

    rt_adc_enable(adc_dev, ADC_DEV_CHANNEL);

    while(1)
    {
        t=rt_adc_read(adc_dev, ADC_PIN);
        if(t>3000 && t<4000)
        {
            rt_pin_write(LED_PIN, 1);
        }
        else {
            rt_pin_write(LED_PIN, 0);
        }
    }
}
